#include <iostream>
#include <stdlib.h>

using namespace std;

struct Nodo
{
	int dato;
	Nodo* siguiente;
};

/*
	Pasos para insertar elementos en una pila. 
	1. Crear el espacio en memoria para almacenar un nodo. 
	2. Cargar el valor dentro del nodo (dato). 
	3. Cargar el puntero dentro del nodo(*siguiente).
	4. Asignar el nuevo nodo a pila. 
*/
void PushPila(Nodo *&pila, int n)
{
	Nodo* nuevoNodo = new Nodo(); //Paso 1
	nuevoNodo->dato = n; //Paso 2
	nuevoNodo->siguiente = pila; //Paso 3
	pila = nuevoNodo;//Paso 4

	cout << "Dato agregado " << n << "\n";
}

/*
	Pasos para sacar elementos de una pila. 
	1. Crear una variable *aux de tipo Nodo. 
	2. Igualar "n" a aux->dato;
	3. Pasar "pila" al siguiente nodo. 
	4. Eliminar aux. 


*/
void PopPila(Nodo *&pila, int &n)
{
	Nodo* aux = pila;//Paso 1. 
	n = aux->dato; //Paso 2.
	pila = aux->siguiente;// Paso 3. 
	//por eso necesitamos una variable aux. Para mantener una referencia para luego eliminarlo. 
	delete aux;
}


int main()
{
	Nodo* pila = NULL;

	int dato;

	cout << "Digite un numero :";
	cin >> dato;

	PushPila(pila, dato);

	cout << "\n Digite un numero :";
	cin >> dato;

	PushPila(pila, dato);

	cout << "\nSacando los elementeos de la pila: ";

	while (pila != NULL)//Mientras non sea el final de la pila
	{
		PopPila(pila, dato);

		if (pila != NULL)
		{
			cout << dato << " , ";
		}
		else
		{
			cout << dato << ".";
		}
	}
}

